import axios from 'axios';
import React, {useEffect, useState} from 'react';
import Houses_Gallery from "./Houses_Gallery";


export default function Houses() {
    const [localhouses, setLocalhouses] = useState([])


    useEffect(() => {
            axios.get(process.env.REACT_APP_BACK_URL + 'houses').then(res => {
                    console.log(res)
                    setLocalhouses(res.data)
                }
            )
        }
    )

    return (
        <Houses_Gallery houses={localhouses}/>
    );
}




