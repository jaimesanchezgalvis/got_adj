import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import './List.scss';
import {Link, Route, Switch} from "react-router-dom";


export default function Houses_Gallery(props) {
    return (
        <div className="menu">
            <div className="Container_big">
                <div className="Container_gallery">
                    <div className="Container">
                        <div className="row">
                            {props.houses.map((houses, i) =>
                                <div className={"col-lg-2 col-md-4 col-sm-12"}>
                                    <Link to={"/house/detail/" + houses.name}>
                                        <img className="house_image"
                                             src={houses.logoURL ? houses.logoURL : 'houses.jpg'} alt=""/>
                                        <p className="Rectangle-h">{houses.name}</p>
                                    </Link>
                                </div>
                            )}
                        </div>
                    </div>

                </div>

            </div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <span><Link to="/characters">Personajes</Link></span>
                        </li>
                        <li className="nav-item">
                            <span><Link to="/houses">Casas</Link></span>
                        </li>
                        <li className="nav-item">
                            <span><Link to="/chronology">Cronologia</Link></span>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

    )
}
