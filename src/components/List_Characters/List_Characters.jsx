import axios from 'axios';
import React, {useEffect, useState} from 'react';
import Characters_Gallery from "./Characters_Gallery";
import Navigation from "../Navigation/Navigation";


export default function Characters() {
    const [localcharacters, setLocalcharacters] = useState([])

    useEffect(() => {
            axios.get(process.env.REACT_APP_BACK_URL + 'characters').then(resp => {
                    console.log(resp)
                    setLocalcharacters(resp.data)
                }
            )
        }
    )

    return (
        <Characters_Gallery characters={localcharacters}/>
    );
}
