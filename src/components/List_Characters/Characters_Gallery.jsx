import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import './Characters_Gallery.scss';
import {Link, Route, Switch} from "react-router-dom";


export default function characters(props) {
    return (
        <div className="menu">
            <div className="Container_big">
                <div className="Container_gallery">
                    <div className="row">
                        {props.characters.map((characters, i) =>
                            <div className={"col-lg-2 col-md-4 col-sm-12"}>
                                <Link to={"/character/detail/" + characters.name}>
                                    <img className="character_image" src={characters.image} alt=""/>
                                    <p className="Rectangle-p">{characters.name}</p>
                                </Link>
                            </div>
                        )}
                    </div>
                </div>
            </div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <span><Link to="/characters">Personajes</Link></span>
                        </li>
                        <li className="nav-item">
                            <span><Link to="/houses">Casas</Link></span>
                        </li>
                        <li className="nav-item">
                            <span><Link to="/chronology">Cronologia</Link></span>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    )
}