import React from "react";
import List from "./components/List_Houses/List";
import Translate from "./components/Translate/Translate";
import Detail from "./components/Detail/Detail";
import Navigation from "./components/Navigation/Navigation";
import List_Characters from "./components/List_Characters/List_Characters";
import { Homepage } from "./pages/Homepage/Homepage";
import ChronologyPage from "./pages/ChronologyPage/ChronologyPage";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import DetailCharacterPage from "./pages/DetailPage/pages/DetailCharacterPage/DetailCharacterPage";
import DetailHousePAge from "./pages/DetailPage/pages/DetailHousePage/DetailHousePage";


function Routes() {
    return (
        <Router>
            <Switch>
                <Route path='/Detail' component={Detail}/>*/}
                <Route path='/chronology'><ChronologyPage/></Route>
{/*                <Route path='/avigation' component={Navigation}/>*/}


                <Route path='/house/detail/:name' component={ DetailHousePAge }/>



                <Route path='/houses' component={List}/>
!
!
                <Route path='/character/detail/:name' component={ DetailCharacterPage }/>





                <Route path='/characters' component={List_Characters}/>


{/*                <Route path='/translate' component={Translate}/>*/}
                <Route path='/'><Homepage/></Route>


            </Switch>
        </Router>
    );
}

export default Routes;
