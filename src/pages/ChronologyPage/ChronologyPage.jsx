import React, {useEffect, useState} from "react";
import axios from 'axios';
import ChronologyGallery from "./components/ChronologyGallery/ChronologyGallery";

export default function ChronologyPage() {

    const [charactersByAge, setCharactersByAge] = useState([]);

    useEffect(() => {
            axios.get(process.env.REACT_APP_BACK_URL + 'characters').then(res => {
                    console.log(res)
                    setCharactersByAge(res.data);
            })

        }, [])

    return (
        <div>
            <ChronologyGallery character={charactersByAge}/>
        </div>
    );
}
