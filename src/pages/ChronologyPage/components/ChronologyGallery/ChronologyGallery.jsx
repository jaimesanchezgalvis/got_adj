import React from "react";

export default function ChronologyGallery(props) {

    return (
        <div>
            <div>
                <div>
                    <button>^</button>
                    <button>v</button>
                    {props.character.map((character,  i) =>
                        <div>
                           {/* <p>{character.age.age}</p>*/}
                            <p>{character.name}</p>
                            <img className="character_image" src={character.image}/>
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}
