import React from 'react';
import background from "./assets/image1.svg";
import './Homepage.scss';


export function Homepage() {
    return (
        <div className="col-12 container">
            <img src={background} className="image-1"/>
            <div className="col-12 Games-of-Thrones">
                <h1>GAME OF THRONES</h1>
            </div>
        </div>
    );
}
