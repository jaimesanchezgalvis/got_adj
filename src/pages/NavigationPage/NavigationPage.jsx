import React from 'react';
import './NavigationPage.scss';


export function NavigationPage() {
    return (
        <div className="Container_big">
            <div className="col-12 container">
                <ul>
                    <li>Personajes</li>
                    <li>Casas</li>
                    <li>Cronologia</li>
                </ul>
            </div>
        </div>
    );
}