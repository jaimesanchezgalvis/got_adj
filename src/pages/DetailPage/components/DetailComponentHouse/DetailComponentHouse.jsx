import React from 'react';


function DetailComponentHouse(props){

    const detail = props.detail;

    return (
        <div>
            <div>
                <div>
                    <figure>
                        <img src={detail.logoURL}/>
                        <figcaption>
                            <h3>{detail.name}</h3>
                        </figcaption>
                    </figure>
                </div>
            </div>
            <div className="container text">
                <div>
                    <h3>LEMA</h3>
                    {/* Preguntar por que no pinta la imagen */}
                    <img src={props.logoCasa}/>
                </div>

                <div>
                    <h3>SEDE</h3>
                    {detail.allegiances.map( (allegiance, i) =>  <p key={i}>{allegiance}</p>) }
                </div>
                
                <div>
                    <h3>REGION</h3>
                    {detail.appearances.map( (appearance, i) =>  <p key={i}>{appearance}</p>) }
                </div>
                
                <div>
                    <h3>ALIANZAS</h3>
                    <p>{detail.father}</p>
                </div>
                
                <div>
                    <h3>RELIGIONES</h3>
                    {detail.siblings.map( (sibling, i) =>  <p key={i}>{sibling}</p>) }
                </div>
                
                <div>
                    <h3>FUNDACION</h3>
                    {detail.titles.map( (title, i) =>  <p key={i}>{title}</p>) }
                </div>
            
            </div>
        </div>
    );
}

export default DetailComponentHouse;