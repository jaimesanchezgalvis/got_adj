import React from 'react';
import './DetailComponentCharacter.scss';

function DetailComponentCharacter(props){

    const detail = props.detail;

    return (
        <div>
            <div>
                <div>
                    <figure>
                        <img src={detail.image}/>
                        <figcaption>
                            <h3>{detail.name}</h3>
                        </figcaption>
                    </figure>
                </div>
            </div>
            <div className="container text">
                <div>
                    <h3>Casa</h3>
                    {/* Preguntar por que no pinta la imagen */}
                    <img src={props.logoCasa}/>
                </div>

                <div>
                    <h3>Alianzas</h3>
                    {detail.allegiances.map( (allegiance, i) =>  <p key={i}>{allegiance}</p>) }
                </div>
                
                <div>
                    <h3>Apariciones</h3>
                    {detail.appearances.map( (appearance, i) =>  <p key={i}>{appearance}</p>) }
                </div>
                
                <div>
                    <h3>Padre</h3>
                    <p>{detail.father}</p>
                </div>
                
                <div>
                    <h3>Hermanos</h3>
                    {detail.siblings.map( (sibling, i) =>  <p key={i}>{sibling}</p>) }
                </div>
                
                <div>
                    <h3>Titulos</h3>
                    {detail.titles.map( (title, i) =>  <p key={i}>{title}</p>) }
                </div>
            
            </div>
        </div>
    );
}

export default DetailComponentCharacter;