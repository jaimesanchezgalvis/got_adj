import React from 'react';
import {useParams} from 'react-router-dom';
import { useState, useEffect } from 'react';
import axios from 'axios';
import DetailComponentHouse from '../../components/DetailComponentHouse/DetailComponentHouse';
import DetailComponentCharacter from '../../components/DetailComponentCharacter/DetailComponentCharacter';

function DetailHousePAge(){

    //const name = useParams().name;
    const name = 'House Stark';

    const [detailHouse, setDetailHouse] = useState(null);

    useEffect( ()=>{
        axios.get(process.env.REACT_APP_BACK_URL + 'houses/' + name)
            .then( (res)=>{
                console.log(res.data);
                setDetailHouse(res.data);
            });
    }, []);

    return (
        <div>
            {/* {detailHouse && <DetailComponentHouse detail={detailHouse}/>} */}
        </div>
    );

}

export default DetailHousePAge;